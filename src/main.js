var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  wall: [64, 64, 64],
  flood: [255, 255, 255]
}

var boardSize
var size = 23
var rule = 0
var arrayMaze = create2DArray(size, size, 0, true)
for (var i = 0; i < arrayMaze.length; i++) {
  for (var j = 0; j < arrayMaze[i].length; j++) {
    if (i === 0 || j === 0 || i === arrayMaze.length - 1 || j === arrayMaze[i].length - 1) {
      arrayMaze[i][j] = 1
    }
  }
}
var chamberStack = []
chamberStack.push([0, 0, size - 2, size - 2])
var orientation
var wallPos
var doorPos
var chamberX
var chamberY
var chamberW
var chamberH
var subChambers
var possibleStart = []
var startingPos
var flood = []
var nextStep

var frames = 0
var state = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  while (chamberStack.length > 0) {
    if (chamberChecker(chamberStack[chamberStack.length - 1]) === true) {
      orientation = wallOrientation(chamberStack[chamberStack.length - 1])
      wallPos = wallPosition(chamberStack[chamberStack.length - 1], orientation)
      doorPos = doorPosition(chamberStack[chamberStack.length - 1], orientation)
      chamberX = chamberStack[chamberStack.length - 1][0]
      chamberY = chamberStack[chamberStack.length - 1][1]
      chamberW = chamberStack[chamberStack.length - 1][2]
      chamberH = chamberStack[chamberStack.length - 1][3]
      for (var i = chamberX + 1; i < chamberX + 1 + chamberW; i++) {
        for (var j = chamberY + 1; j < chamberY + 1 + chamberH; j++) {
          if (orientation === 0) {
            arrayMaze[chamberX + wallPos + 1][j] = 1
            arrayMaze[chamberX + wallPos + 1][chamberY + doorPos + 1] = 0
          } else {
            arrayMaze[i][chamberY + wallPos + 1] = 1
            arrayMaze[chamberX + doorPos + 1][chamberY + wallPos + 1] = 0
          }
        }
      }
      subChambers = getSubChambers(chamberStack[chamberStack.length - 1], orientation, wallPos, chamberX, chamberY, chamberW, chamberH)
      chamberStack.push(subChambers[0])
      chamberStack.push(subChambers[1])
      chamberStack.splice(-3, 1)
    } else {
      chamberStack.splice(-1, 1)
    }
  }
  for (var i = 0; i < arrayMaze.length; i++) {
    for (var j = 0; j < arrayMaze[i].length; j++) {
      if (arrayMaze[i][j] !== 1) {
        possibleStart.push([i, j])
      }
    }
  }
  startingPos = possibleStart[Math.floor(Math.random() * possibleStart.length)]
  arrayMaze[startingPos[0]][startingPos[1]] = 3
  flood.push(startingPos)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < arrayMaze.length; i++) {
    for (var j = 0; j < arrayMaze[i].length; j++) {
      noStroke()
      if (arrayMaze[i][j] === 1) {
        if (state === 0) {
          fill(colors.wall)
        } else {
          noFill()
        }
      } else if (arrayMaze[i][j] === 0) {
        fill(colors.dark)
      } else {
        fill(colors.flood[0] * (arrayMaze[i][j] - 2), colors.flood[1] * (arrayMaze[i][j] - 2), colors.flood[2] * (arrayMaze[i][j] - 2))
      }
      rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size))
    }
  }

  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0
    nextStep = getNeighbours(flood)
    for (var i = 0; i < nextStep.length; i++) {
      arrayMaze[nextStep[i][0]][nextStep[i][1]] = 3
      flood.push(nextStep[i])
    }
    nextStep = null
    for (var i = 0; i < arrayMaze.length; i++) {
      for (var j = 0; j < arrayMaze.length; j++) {
        if (arrayMaze[i][j] > 2) {
          arrayMaze[i][j] -= 0.05
        } else if (arrayMaze[i][j] !== 1 && arrayMaze[i][j] !== 0) {
          arrayMaze[i][j] = 2
        }
      }
    }
    if (Math.max(...arrayMaze.flat()) === 2) {
      if (state === 0) {
        state = 1
      } else {
        state = 0
        arrayMaze = create2DArray(size, size, 0, true)
        for (var i = 0; i < arrayMaze.length; i++) {
          for (var j = 0; j < arrayMaze[i].length; j++) {
            if (i === 0 || j === 0 || i === arrayMaze.length - 1 || j === arrayMaze[i].length - 1) {
              arrayMaze[i][j] = 1
            }
          }
        }
        flood = []
        chamberStack = []
        chamberStack.push([0, 0, size - 2, size - 2])
        while (chamberStack.length > 0) {
          if (chamberChecker(chamberStack[chamberStack.length - 1]) === true) {
            orientation = wallOrientation(chamberStack[chamberStack.length - 1])
            wallPos = wallPosition(chamberStack[chamberStack.length - 1], orientation)
            doorPos = doorPosition(chamberStack[chamberStack.length - 1], orientation)
            chamberX = chamberStack[chamberStack.length - 1][0]
            chamberY = chamberStack[chamberStack.length - 1][1]
            chamberW = chamberStack[chamberStack.length - 1][2]
            chamberH = chamberStack[chamberStack.length - 1][3]
            for (var i = chamberX + 1; i < chamberX + 1 + chamberW; i++) {
              for (var j = chamberY + 1; j < chamberY + 1 + chamberH; j++) {
                if (orientation === 0) {
                  arrayMaze[chamberX + wallPos + 1][j] = 1
                  arrayMaze[chamberX + wallPos + 1][chamberY + doorPos + 1] = 0
                } else {
                  arrayMaze[i][chamberY + wallPos + 1] = 1
                  arrayMaze[chamberX + doorPos + 1][chamberY + wallPos + 1] = 0
                }
              }
            }
            subChambers = getSubChambers(chamberStack[chamberStack.length - 1], orientation, wallPos, chamberX, chamberY, chamberW, chamberH)
            chamberStack.push(subChambers[0])
            chamberStack.push(subChambers[1])
            chamberStack.splice(-3, 1)
          } else {
            chamberStack.splice(-1, 1)
          }
        }
        for (var i = 0; i < arrayMaze.length; i++) {
          for (var j = 0; j < arrayMaze[i].length; j++) {
            if (arrayMaze[i][j] !== 1) {
              possibleStart.push([i, j])
            }
          }
        }
        startingPos = possibleStart[Math.floor(Math.random() * possibleStart.length)]
        arrayMaze[startingPos[0]][startingPos[1]] = 3
        flood.push(startingPos)
      }
      for (var i = 0; i < flood.lenght; i++) {
        arrayMaze[flood[i][0]][flood[i][1]] = 0
      }
      flood = []
      arrayMaze[startingPos[0]][startingPos[1]] = 3
      flood.push(startingPos)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function getNeighbours(flood) {
  var neighbours = []
  for (var i = 0; i < flood.length; i++) {
    var posX = flood[i][0]
    var posY = flood[i][1]
    if (posX > 0) {
      if (isArrayInArray(flood, [posX - 1, posY]) === false && arrayMaze[posX - 1][posY] !== 1) {
      neighbours.push([posX - 1, posY])
      }
    }
    if (posX < size - 1) {
      if (isArrayInArray(flood, [posX + 1, posY]) === false && arrayMaze[posX + 1][posY] !== 1) {
        neighbours.push([posX + 1, posY])
      }
    }
    if (posY > 0) {
      if (isArrayInArray(flood, [posX, posY - 1]) === false && arrayMaze[posX][posY - 1] !== 1) {
        neighbours.push([posX, posY - 1])
      }
    }
    if (posY < size - 1) {
      if (isArrayInArray(flood, [posX, posY + 1]) === false && arrayMaze[posX][posY + 1] !== 1) {
        neighbours.push([posX, posY + 1])
      }
    }
  }
  neighbours = multiDimensionalUnique(neighbours)
  return neighbours
}

function multiDimensionalUnique(arr) {
  var uniques = []
  var itemsFound = {}
  for (var i = 0, l = arr.length; i < l; i++) {
    var stringified = JSON.stringify(arr[i])
    if (itemsFound[stringified]) {
      continue
    }
    uniques.push(arr[i])
    itemsFound[stringified] = true
  }
  return uniques
}

function isArrayInArray(arr, item) {
  var item_as_string = JSON.stringify(item)
  var contains = arr.some(function(ele) {
    return JSON.stringify(ele) === item_as_string
  })
  return contains
}

function chamberChecker(stack) {
  var dividable
  var width = stack[2]
  var height = stack[3]
  if (width < 3 + rule || height < 3 + rule) {
    dividable = false
  } else {
    dividable = true
  }
  return dividable
}

function wallOrientation(stack) {
  var orientation
  var width = stack[2]
  var height = stack[3]
  if (width > height) {
    orientation = 0
  } else if (width < height) {
    orientation = 1
  } else if (width === height) {
    orientation = Math.floor(Math.random() * 2)
  }
  return orientation
}

function wallPosition(stack, orientation) {
  var position
  if (orientation === 0) {
    position = 1 + Math.floor(Math.random() * (stack[2] - rule) * 0.5) * 2
  } else {
    position = 1 + Math.floor(Math.random() * (stack[3] - rule) * 0.5) * 2
  }
  return position
}

function doorPosition(stack, orientation) {
  var position
  if (orientation === 0) {
    position = Math.floor(Math.random() * (stack[3] - rule) * 0.5) * 2
  } else {
    position = Math.floor(Math.random() * (stack[2] - rule) * 0.5) * 2
  }
  return position
}

function getSubChambers(stack, orientation, wallPos, chamberX, chamberY, chamberW, chamberH) {
  var subChamberA
  var subChamberB
  if (orientation === 0) {
    subChamberA = [chamberX, chamberY, wallPos, chamberH]
    subChamberB = [chamberX + wallPos + 1, chamberY, chamberW - wallPos - 1, chamberH]
  } else {
    subChamberA = [chamberX, chamberY, chamberW, wallPos]
    subChamberB = [chamberX, chamberY + wallPos + 1, chamberW, chamberH - wallPos - 1]
  }
  return [subChamberA, subChamberB]
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
